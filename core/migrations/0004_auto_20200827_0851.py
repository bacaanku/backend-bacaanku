# Generated by Django 3.0.9 on 2020-08-27 08:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20200824_0759'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bookinteracted',
            name='gPrintedPageCount',
        ),
        migrations.RemoveField(
            model_name='bookinteracted',
            name='pageRead',
        ),
    ]
