from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class UserDetail(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)

class BookInteracted(models.Model):
    BOOK_STATUS_CHOICES = [
        ('wishlist', 'wishlist'),
        ('onread', 'onread'),
        ('read','read')
    ]
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.CharField(max_length=8, choices=BOOK_STATUS_CHOICES)
    gId = models.CharField(max_length=20)
    gSelfLink = models.CharField(max_length=200)
    gThumbnail = models.CharField(max_length=400, default=None, blank=True, null=True)
    gAuthor = models.CharField(max_length=200, default=None)
    gTitle = models.CharField(max_length=200, default=None)