from rest_framework import serializers

from django.contrib.auth.models import User
from .models import UserDetail, BookInteracted


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'username', 'password', 'id']
        extra_kwargs = {
            'password': {'write_only': True},
            'id': {'read_only': True}
        }
    
    def create(self, validated_data):
        user_object = User(email=validated_data['email'], username=validated_data['username'])
        user_object.set_password(validated_data['password'])
        user_object.save()

        user_detail_object = UserDetail(user=user_object)
        user_detail_object.save()
        
        return user_object
    
    def validate_password(self, value):
        if len(value) < 8:
            raise serializers.ValidationError('Password must be equal or longer than 8 characters.')
        return value


class BookInteractedSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    user = UserSerializer(read_only=True)
    status = serializers.CharField(max_length=8)
    gId = serializers.CharField(max_length=20)
    gSelfLink = serializers.CharField(max_length=200)
    gThumbnail = serializers.CharField(max_length=400, required=False)
    gAuthor = serializers.CharField(max_length=200)
    gTitle = serializers.CharField(max_length=200)

    def create(self, validated_data, user_object):
        book_interacted_object = BookInteracted(user=user_object, **validated_data)
        book_interacted_object.save()
        return book_interacted_object
    
    def update(self, instance, validated_data):
        if 'status' in validated_data:
            instance.status = validated_data['status']
        instance.save()
        return instance
