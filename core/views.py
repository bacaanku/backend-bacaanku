from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

from .models import BookInteracted
from django.contrib.auth.models import User
from .serializers import UserSerializer, BookInteractedSerializer

# Create your views here.
@api_view(['POST'])
def register(request):
    data = JSONParser().parse(request)
    userSerializer = UserSerializer(data=data)
    if userSerializer.is_valid(raise_exception=True):
        userSerializer.create(data)
        return Response(userSerializer.data)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def test(request):
    print(request.headers['Authorization'])
    print(request.user)
    return HttpResponse('oke!')

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def addBookInteracted(request):
    user_object = User.objects.get(username=request.user.username)

    data = JSONParser().parse(request)
    book_interacted_serializer = BookInteractedSerializer(data=data)
    if book_interacted_serializer.is_valid(raise_exception=True):
        book_interacted_serializer.create(data, user_object)
        return Response(book_interacted_serializer.data)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def bookInteractedList(request):
    if request.user.is_superuser:
        return bookInteractedListAdmin(request)
    
    user_object = User.objects.get(username=request.user.username)
    books_interacted_queryset = BookInteracted.objects.filter(user=user_object)

    if 'status' in request.query_params:
        books_interacted_queryset = books_interacted_queryset.filter(status=request.query_params['status'])

    books_interacted_serializer = BookInteractedSerializer(books_interacted_queryset, many=True)
    return Response(books_interacted_serializer.data)

def bookInteractedListAdmin(request):
    books_interacted_queryset = BookInteracted.objects.all()
    if 'status' in request.query_params:
        books_interacted_queryset = books_interacted_queryset.filter(status=request.query_params['status'])
    book_interacted_serializer = BookInteractedSerializer(books_interacted_queryset, many=True)
    return Response(book_interacted_serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticated])
def bookInteracted(request, book_pk):
    user_object = User.objects.get(username=request.user.username)

    try:
        book_object = BookInteracted.objects.get(pk=book_pk, user=user_object)
    except BookInteracted.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        book_serializer = BookInteractedSerializer(book_object)
        return Response(book_serializer.data)
    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        book_serializer = BookInteractedSerializer(book_object, data, partial=True)
        if book_serializer.is_valid(raise_exception=True):
            book_serializer.update(book_object, data)
            return Response(book_serializer.data)
    else:
        book_object.delete()
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)