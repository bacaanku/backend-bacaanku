from django.contrib import admin
from .models import UserDetail, BookInteracted
# Register your models here.

admin.site.register(UserDetail)
admin.site.register(BookInteracted)