from django.urls import path

from . import views

urlpatterns = [
    path('register', views.register, name='register'),
    path('auth-test', views.test, name='test'),
    path('add-book-interacted', views.addBookInteracted, name='add-book-interacted'),
    path('book-interacted-list', views.bookInteractedList, name='book-interacted-list'),
    path('book-interacted/<int:book_pk>', views.bookInteracted, name='book-interacted')
]